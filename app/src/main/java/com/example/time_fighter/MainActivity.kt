package com.example.time_fighter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var startButton: Button
    private lateinit var playerName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        startButton = findViewById(R.id.btn_start)
        playerName = findViewById(R.id.txt_name_player)
        startButton.setOnClickListener{showGameActivity()}

    }

    private fun showGameActivity(){
        val gameActivityIntent = Intent (this, GameActivity::class.java)

        gameActivityIntent.putExtra(INTENT_PLAYER_NAME, playerName.text.toString())
        startActivity(gameActivityIntent)

    }


    companion object{
        const val INTENT_PLAYER_NAME = "playerName"
    }
}
