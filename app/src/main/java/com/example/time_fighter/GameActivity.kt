package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class GameActivity : AppCompatActivity() {
    
    private lateinit var ativity_title: TextView
    private lateinit var playerName: String

    private lateinit var gameScoreTextView: TextView
    private lateinit var timeLeftTextView: TextView
    private lateinit var tapMeButton: Button

    private lateinit var countDownTimer: CountDownTimer
    private var initialCountDownTimer: Long = 10000  //Milisegundos
    private var countDownInterval: Long = 1000
    private var timeLeft = 10

    private var gameScore = 0
    private var isGameStarted = false

    private val TAG = GameActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)


        Log.d(TAG,"onCreate called , score is $gameScore")

        ativity_title = findViewById(R.id.txt_game)
        gameScoreTextView = findViewById(R.id.lbl_score)
        timeLeftTextView = findViewById(R.id.lbl_time)
        tapMeButton = findViewById(R.id.btn_tap)

        playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME)?:"ERROR"

        ativity_title.text = getString(R.string.get_ready_player, playerName)

        tapMeButton.setOnClickListener{incrementScore()}

        if (savedInstanceState != null){

            gameScore = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            isGameStarted = savedInstanceState.getBoolean(GAME_STARTED)

            restoreGame()
        }else {

            resetGame()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SCORE_KEY, gameScore)
        outState.putInt(TIME_LEFT_KEY, timeLeft)
        outState.putBoolean(GAME_STARTED,isGameStarted)


        countDownTimer.cancel()
        Log.d(TAG, "onSaveInstanceState : Savig score : $gameScore and timeLeft: $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG , "onDestroy called")
        // cuando se ejecuta
    }

    private fun incrementScore(){
        if(!isGameStarted){
            startGame()
        }
        gameScore++
        gameScoreTextView.text = getString(R.string.score_label, gameScore)
    }

    private fun resetGame(){
        gameScore = 0
        gameScoreTextView.text = getString(R.string.score_label, gameScore)

        timeLeft = 10
        timeLeftTextView.text = getString(R.string.time_label, timeLeft)

        countDownTimer = object : CountDownTimer(initialCountDownTimer, countDownInterval){ //clase concreta, se puede implementar los metodos
            override fun onFinish() {  // se jecuta cuando llegue a 0
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {  // se jecuta cada segundo
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_label, timeLeft)
            }

        }
        isGameStarted = false
    }

    private fun startGame(){
        countDownTimer.start()
        isGameStarted = true
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
        resetGame()
    }
    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score_label,gameScore)
        timeLeftTextView.text = getString(R.string.time_label, timeLeft)

        countDownTimer = object : CountDownTimer((timeLeft* 1000).toLong(), countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/ 1000
                timeLeftTextView.text = getString(R.string.time_label, timeLeft)
            }

            override fun onFinish() {
                endGame()
            }

        }
        countDownTimer.start()
        isGameStarted = true
    }

    private fun configCountDownTimer(){

        countDownTimer = object : CountDownTimer((timeLeft* 1000).toLong(), countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_label, timeLeft)
            }

            override fun onFinish() {
                endGame()
            }
        }
    }

    companion object{
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private const val GAME_STARTED = "GAME_STARTED"

    }
}
